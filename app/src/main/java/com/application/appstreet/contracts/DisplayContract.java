package com.application.appstreet.contracts;

import com.application.appstreet.model.Photo;
import com.application.appstreet.presenters.BasePresenter;
import com.application.appstreet.views.BaseView;

import java.util.List;

/**
 * Created by harsh.jain on 4/25/18.
 */

public class DisplayContract {

    public interface View extends BaseView {
        void displayImage(List<Photo> photos, int position);
    }

    public interface Presenter extends BasePresenter<DisplayContract.View> {
        void displayImage(List<Photo> photos, int position);
    }
}
