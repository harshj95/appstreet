package com.application.appstreet.contracts;

import android.content.Context;
import android.graphics.Bitmap;

import com.application.appstreet.model.Photo;
import com.application.appstreet.presenters.BasePresenter;
import com.application.appstreet.views.BaseView;

import java.util.List;

/**
 * Created by harsh.jain on 4/24/18.
 */

public class MainContract {

    public interface View extends BaseView {
        void displayImages(List<Photo> photos);
        void displayPagedImages(List<Photo> photos);
        void displayOfflineImages(List<String> photoUrls);
        void showErrorMessage(String message);
        void changeColumns(int columns);
        void displayImage(int position);
        void hideKeyboard();
    }

    public interface Presenter extends BasePresenter<View> {
        void getImages(String text, int page, int count);
        void getOfflineImages(Context context, String text);
        void displayImage(int position);
    }
}
