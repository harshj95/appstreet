package com.application.appstreet.util;

/**
 * Created by harsh.jain on 4/24/18.
 */

public class Constants {

    public static final String APP_PREFERENCE = "appstreet_settings";

    public static final String METHOD = "flickr.photos.search";
    public static final String API_KEY = "8ccb5b3a0d00805d50caceff3d9f44d8";
    public static final String EXTRAS = "url_m";
    public static final String FORMAT = "json";

    public static final String IMAGES = "images";
    public static final String POSITION = "position";
    public static final int RESULT_CODE = 100;
    public static final int REQUEST_CODE = 200;

    public static final String DISK_PHOTO = "photoSavedtoDisk";
}
