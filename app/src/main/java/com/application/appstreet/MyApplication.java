package com.application.appstreet;

import android.app.Application;
import android.graphics.Bitmap;

import com.application.appstreet.util.LruCache;

/**
 * Created by harsh.jain on 4/25/18.
 */

public class MyApplication extends Application {

    public LruCache cache;

    public void onCreate() {
        super.onCreate();
        cache = new LruCache<String, Bitmap>(30);
    }

    @Override
    public void onLowMemory() {
        cache.clear();
        super.onLowMemory();
    }
}
