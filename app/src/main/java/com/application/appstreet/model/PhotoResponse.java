package com.application.appstreet.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by harsh.jain on 4/23/18.
 */

public class PhotoResponse {

    @SerializedName("photos")
    @Expose
    private PhotoData data;
    @SerializedName("stat")
    @Expose
    private String status;

    public PhotoData getData() {
        return data;
    }

    public void setData(PhotoData data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

