package com.application.appstreet.views;

/**
 * Created by harsh.jain on 4/23/18.
 */

public interface BaseView {
    void showProgress();
    void hideProgress();
}
