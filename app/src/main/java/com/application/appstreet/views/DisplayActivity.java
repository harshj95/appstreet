package com.application.appstreet.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;

import com.application.appstreet.R;
import com.application.appstreet.adapters.FullscreenImageAdapter;
import com.application.appstreet.contracts.DisplayContract;
import com.application.appstreet.model.Photo;
import com.application.appstreet.presenters.DisplayPresenter;
import com.application.appstreet.util.Constants;

import java.util.List;

import butterknife.BindView;

/**
 * Created by harsh.jain on 4/25/18.
 */

public class DisplayActivity extends BaseActivity<DisplayContract.Presenter> implements DisplayContract.View {

    @BindView(R.id.pager)
    protected ViewPager mViewPager;
    private FullscreenImageAdapter mFullscreenImageAdapter;
    private Context mContext;

    @NonNull
    @Override
    protected DisplayContract.Presenter getPresenterInstance() {
        return new DisplayPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        mContext = getApplicationContext();

        supportPostponeEnterTransition();

        mContext = getApplicationContext();

        Bundle bundle = getIntent().getExtras();
        List<Photo> photos = bundle.getParcelableArrayList(Constants.IMAGES);
        int position = getIntent().getIntExtra(Constants.POSITION, 0);

        mPresenter.displayImage(photos, position);
    }

    @Override
    public void displayImage(List<Photo> photos, int position) {
        mFullscreenImageAdapter = new FullscreenImageAdapter(this,
                photos, mContext);

        mViewPager.setAdapter(mFullscreenImageAdapter);

        mViewPager.setCurrentItem(position);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(Constants.POSITION, mViewPager.getCurrentItem());
        setResult(Constants.RESULT_CODE, intent);
        finish();
    }
}
