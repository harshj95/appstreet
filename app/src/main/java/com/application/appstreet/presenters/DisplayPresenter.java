package com.application.appstreet.presenters;

import com.application.appstreet.contracts.DisplayContract;
import com.application.appstreet.model.Photo;

import java.util.List;

/**
 * Created by harsh.jain on 4/25/18.
 */

public class DisplayPresenter extends BasePresenterImpl<DisplayContract.View> implements DisplayContract.Presenter {

    private DisplayContract.View mView;

    public DisplayPresenter(DisplayContract.View view)
    {
        this.mView = view;
    }

    @Override
    public void displayImage(List<Photo> photos, int position) {
        mView.displayImage(photos, position);
    }
}
