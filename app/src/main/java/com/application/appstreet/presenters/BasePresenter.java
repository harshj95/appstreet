package com.application.appstreet.presenters;

import com.application.appstreet.views.BaseView;

/**
 * Created by harsh.jain on 4/23/18.
 */

public interface BasePresenter<V extends BaseView> {

    void attachView(V view);

    void detachView();
}